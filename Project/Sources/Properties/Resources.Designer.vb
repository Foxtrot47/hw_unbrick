Imports System
Imports System.CodeDom.Compiler
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Drawing
Imports System.Globalization
Imports System.Resources
Imports System.Runtime.CompilerServices

Namespace Reverse_Tool.Properties
	<CompilerGenerated>
	<DebuggerNonUserCode>
	<GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")>
	Friend Class Resources
		Private Shared resourceMan As System.Resources.ResourceManager

		Private Shared resourceCulture As CultureInfo

		Friend Shared ReadOnly Property C4 As Byte()
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("C4", Resources.resourceCulture), Byte())
			End Get
		End Property

		<EditorBrowsable(EditorBrowsableState.Advanced)>
		Friend Shared Property Culture As CultureInfo
			Get
				Return Resources.resourceCulture
			End Get
			Set(ByVal value As CultureInfo)
				Resources.resourceCulture = value
			End Set
		End Property

		Friend Shared ReadOnly Property cyber As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("cyber", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Erase22 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Erase22", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Erasefrp As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Erasefrp", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Factory As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Factory", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Format22 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Format22", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property logs As String
			Get
				Return Resources.ResourceManager.GetString("logs", Resources.resourceCulture)
			End Get
		End Property

		Friend Shared ReadOnly Property logs1 As String
			Get
				Return Resources.ResourceManager.GetString("logs1", Resources.resourceCulture)
			End Get
		End Property

		Friend Shared ReadOnly Property MiCloud22 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("MiCloud22", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property miremof As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("miremof", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property OpenLock As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("OpenLock", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property process As String
			Get
				Return Resources.ResourceManager.GetString("process", Resources.resourceCulture)
			End Get
		End Property

		Friend Shared ReadOnly Property Readinfo22 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Readinfo22", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		<EditorBrowsable(EditorBrowsableState.Advanced)>
		Friend Shared ReadOnly Property ResourceManager As System.Resources.ResourceManager
			Get
				If (Resources.resourceMan Is Nothing) Then
					Resources.resourceMan = New System.Resources.ResourceManager("Reverse_Tool.Properties.Resources", GetType(Resources).Assembly)
				End If
				Return Resources.resourceMan
			End Get
		End Property

		Friend Shared ReadOnly Property Safe22 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Safe22", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Safe22px As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Safe22px", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Save22 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Save22", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Save22px As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Save22px", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Spdata As String
			Get
				Return Resources.ResourceManager.GetString("Spdata", Resources.resourceCulture)
			End Get
		End Property

		Friend Shared ReadOnly Property Stop_30 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Stop_30", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Shared ReadOnly Property Stop30 As Bitmap
			Get
				Return DirectCast(Resources.ResourceManager.GetObject("Stop30", Resources.resourceCulture), Bitmap)
			End Get
		End Property

		Friend Sub New()
			MyBase.New()
		End Sub
	End Class
End Namespace