Imports System

Namespace BismillahAdb
	Public Enum AdbAppType
		Unknown
		System
		Privileged
		ThirdParty
	End Enum
End Namespace