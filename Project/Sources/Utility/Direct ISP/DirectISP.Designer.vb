﻿Imports DevExpress.XtraEditors
Imports System.Windows.Forms
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DirectISP
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DirectISP))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.XtraFlash = New DevExpress.XtraEditors.PanelControl()
        Me.MainTab = New DevExpress.XtraTab.XtraTabControl()
        Me.xtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.panelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonReboot = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelDownload = New DevExpress.XtraEditors.PanelControl()
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button6 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBox1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.labelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBox3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.panelControl7 = New DevExpress.XtraEditors.PanelControl()
        Me.CacheBoxAutoFormatData = New CacheBox()
        Me.CacheBoxCreateDigest = New CacheBox()
        Me.CekAutoRebootQc = New CacheBox()
        Me.Btn_PatchXML = New System.Windows.Forms.Button()
        Me.labelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.ButtonLoader = New System.Windows.Forms.Button()
        Me.labelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.labelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtFlashPatchXML = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.TxtFlashLoader = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.Btn_RawXML = New System.Windows.Forms.Button()
        Me.TxtFlashRawXML = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.CheckEditAutoXML = New CacheBox()
        Me.ButtonFlashQc = New DevExpress.XtraEditors.SimpleButton()
        Me.ButtonQc_ReadP = New DevExpress.XtraEditors.SimpleButton()
        Me.ButtonQc_eP = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelDg = New DevExpress.XtraEditors.PanelControl()
        Me.CkboxSelectpartitionDataView = New System.Windows.Forms.CheckBox()
        Me.VScrollBarDirectISPFlashDataView = New DevExpress.XtraEditors.VScrollBar()
        Me.HScrollBarDirectISPFlashDataView = New DevExpress.XtraEditors.HScrollBar()
        Me.DataView = New System.Windows.Forms.DataGridView()
        Me.Column4 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Index = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.XtraFlash, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraFlash.SuspendLayout()
        CType(Me.MainTab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainTab.SuspendLayout()
        Me.xtraTabPage1.SuspendLayout()
        CType(Me.panelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelControl1.SuspendLayout()
        CType(Me.PanelDownload, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDownload.SuspendLayout()
        CType(Me.ComboBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.panelControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelControl7.SuspendLayout()
        CType(Me.PanelDg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDg.SuspendLayout()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraFlash
        '
        Me.XtraFlash.Controls.Add(Me.MainTab)
        Me.XtraFlash.Controls.Add(Me.PanelDg)
        Me.XtraFlash.Location = New System.Drawing.Point(0, 0)
        Me.XtraFlash.Name = "XtraFlash"
        Me.XtraFlash.Size = New System.Drawing.Size(653, 482)
        Me.XtraFlash.TabIndex = 3
        '
        'MainTab
        '
        Me.MainTab.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.MainTab.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.MainTab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTab.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left
        Me.MainTab.Location = New System.Drawing.Point(2, 285)
        Me.MainTab.MultiLine = DevExpress.Utils.DefaultBoolean.[True]
        Me.MainTab.Name = "MainTab"
        Me.MainTab.SelectedTabPage = Me.xtraTabPage1
        Me.MainTab.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.[False]
        Me.MainTab.ShowTabHeader = DevExpress.Utils.DefaultBoolean.[True]
        Me.MainTab.Size = New System.Drawing.Size(649, 195)
        Me.MainTab.TabIndex = 29
        Me.MainTab.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.xtraTabPage1})
        '
        'xtraTabPage1
        '
        Me.xtraTabPage1.Appearance.Header.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.xtraTabPage1.Appearance.Header.Options.UseFont = True
        Me.xtraTabPage1.Appearance.Header.Options.UseTextOptions = True
        Me.xtraTabPage1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.xtraTabPage1.Controls.Add(Me.panelControl1)
        Me.xtraTabPage1.Name = "xtraTabPage1"
        Me.xtraTabPage1.Size = New System.Drawing.Size(627, 193)
        Me.xtraTabPage1.Text = "DISK OPERATION"
        '
        'panelControl1
        '
        Me.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.panelControl1.Controls.Add(Me.SimpleButton3)
        Me.panelControl1.Controls.Add(Me.SimpleButtonReboot)
        Me.panelControl1.Controls.Add(Me.PanelDownload)
        Me.panelControl1.Controls.Add(Me.panelControl7)
        Me.panelControl1.Controls.Add(Me.ButtonFlashQc)
        Me.panelControl1.Controls.Add(Me.ButtonQc_ReadP)
        Me.panelControl1.Controls.Add(Me.ButtonQc_eP)
        Me.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelControl1.Location = New System.Drawing.Point(0, 0)
        Me.panelControl1.Name = "panelControl1"
        Me.panelControl1.Size = New System.Drawing.Size(627, 193)
        Me.panelControl1.TabIndex = 0
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton3.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SimpleButton3.Appearance.Options.UseBackColor = True
        Me.SimpleButton3.Appearance.Options.UseTextOptions = True
        Me.SimpleButton3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SimpleButton3.AppearanceHovered.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.AppearanceHovered.Options.UseFont = True
        Me.SimpleButton3.AppearanceHovered.Options.UseImage = True
        Me.SimpleButton3.ImageOptions.Image = Global.Reverse_Tool.My.Resources.Resources.Save22
        Me.SimpleButton3.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.SimpleButton3.Location = New System.Drawing.Point(147, 43)
        Me.SimpleButton3.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.SimpleButton3.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.SimpleButton3.Size = New System.Drawing.Size(131, 28)
        Me.SimpleButton3.TabIndex = 37
        Me.SimpleButton3.Text = "WRITE PARTITION"
        '
        'SimpleButtonReboot
        '
        Me.SimpleButtonReboot.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButtonReboot.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SimpleButtonReboot.Appearance.Font = New System.Drawing.Font("Corbel", 8.25!)
        Me.SimpleButtonReboot.Appearance.Options.UseBackColor = True
        Me.SimpleButtonReboot.Appearance.Options.UseFont = True
        Me.SimpleButtonReboot.Appearance.Options.UseTextOptions = True
        Me.SimpleButtonReboot.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.SimpleButtonReboot.AppearanceHovered.Font = New System.Drawing.Font("Corbel", 9.0!)
        Me.SimpleButtonReboot.AppearanceHovered.Options.UseFont = True
        Me.SimpleButtonReboot.AppearanceHovered.Options.UseImage = True
        Me.SimpleButtonReboot.ImageOptions.Image = Global.Reverse_Tool.My.Resources.Resources.Save22px
        Me.SimpleButtonReboot.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.SimpleButtonReboot.Location = New System.Drawing.Point(408, 43)
        Me.SimpleButtonReboot.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SimpleButtonReboot.Name = "SimpleButtonReboot"
        Me.SimpleButtonReboot.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.SimpleButtonReboot.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.SimpleButtonReboot.Size = New System.Drawing.Size(97, 28)
        Me.SimpleButtonReboot.TabIndex = 36
        Me.SimpleButtonReboot.Text = "READ DUMP"
        '
        'PanelDownload
        '
        Me.PanelDownload.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelDownload.Controls.Add(Me.Button1)
        Me.PanelDownload.Controls.Add(Me.Button6)
        Me.PanelDownload.Controls.Add(Me.LabelControl3)
        Me.PanelDownload.Controls.Add(Me.ComboBox1)
        Me.PanelDownload.Controls.Add(Me.labelControl2)
        Me.PanelDownload.Controls.Add(Me.ComboBox3)
        Me.PanelDownload.Location = New System.Drawing.Point(5, 4)
        Me.PanelDownload.Name = "PanelDownload"
        Me.PanelDownload.Size = New System.Drawing.Size(619, 33)
        Me.PanelDownload.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.Appearance.Options.UseBackColor = True
        Me.Button1.Appearance.Options.UseTextOptions = True
        Me.Button1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Button1.AppearanceHovered.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.Button1.AppearanceHovered.Options.UseFont = True
        Me.Button1.AppearanceHovered.Options.UseImage = True
        Me.Button1.ImageOptions.Image = CType(resources.GetObject("Button1.ImageOptions.Image"), System.Drawing.Image)
        Me.Button1.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.Button1.Location = New System.Drawing.Point(334, 2)
        Me.Button1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Button1.Name = "Button1"
        Me.Button1.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.Button1.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.Button1.Size = New System.Drawing.Size(83, 28)
        Me.Button1.TabIndex = 44
        Me.Button1.Text = "SCAN DISK"
        '
        'Button6
        '
        Me.Button6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button6.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button6.Appearance.Font = New System.Drawing.Font("Corbel", 8.25!)
        Me.Button6.Appearance.Options.UseBackColor = True
        Me.Button6.Appearance.Options.UseFont = True
        Me.Button6.Appearance.Options.UseTextOptions = True
        Me.Button6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.Button6.AppearanceHovered.Font = New System.Drawing.Font("Corbel", 9.0!)
        Me.Button6.AppearanceHovered.Options.UseFont = True
        Me.Button6.AppearanceHovered.Options.UseImage = True
        Me.Button6.ImageOptions.Image = CType(resources.GetObject("Button6.ImageOptions.Image"), System.Drawing.Image)
        Me.Button6.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.Button6.Location = New System.Drawing.Point(231, 2)
        Me.Button6.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Button6.Name = "Button6"
        Me.Button6.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.Button6.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.Button6.Size = New System.Drawing.Size(97, 28)
        Me.Button6.TabIndex = 43
        Me.Button6.Text = "REFRESH DISK"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(6, 9)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl3.TabIndex = 42
        Me.LabelControl3.Text = "Disk"
        '
        'ComboBox1
        '
        Me.ComboBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox1.EditValue = "- Please Refresh Disk -"
        Me.ComboBox1.Location = New System.Drawing.Point(41, 6)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Properties.AllowFocused = False
        Me.ComboBox1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ComboBox1.Properties.Appearance.BorderColor = System.Drawing.Color.DarkRed
        Me.ComboBox1.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Bold)
        Me.ComboBox1.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.ComboBox1.Properties.Appearance.Options.UseBackColor = True
        Me.ComboBox1.Properties.Appearance.Options.UseBorderColor = True
        Me.ComboBox1.Properties.Appearance.Options.UseFont = True
        Me.ComboBox1.Properties.Appearance.Options.UseForeColor = True
        Me.ComboBox1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox1.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.ComboBox1.Properties.Items.AddRange(New Object() {"Qualcomm", "Mediatek", "Spreadtrum"})
        Me.ComboBox1.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.ComboBox1.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ComboBox1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox1.Size = New System.Drawing.Size(184, 20)
        Me.ComboBox1.TabIndex = 41
        '
        'labelControl2
        '
        Me.labelControl2.Location = New System.Drawing.Point(438, 9)
        Me.labelControl2.Name = "labelControl2"
        Me.labelControl2.Size = New System.Drawing.Size(36, 13)
        Me.labelControl2.TabIndex = 40
        Me.labelControl2.Text = "Chipset"
        '
        'ComboBox3
        '
        Me.ComboBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ComboBox3.EditValue = "Qualcomm"
        Me.ComboBox3.Location = New System.Drawing.Point(497, 6)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Properties.AllowFocused = False
        Me.ComboBox3.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ComboBox3.Properties.Appearance.BorderColor = System.Drawing.Color.DarkRed
        Me.ComboBox3.Properties.Appearance.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Bold)
        Me.ComboBox3.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.ComboBox3.Properties.Appearance.Options.UseBackColor = True
        Me.ComboBox3.Properties.Appearance.Options.UseBorderColor = True
        Me.ComboBox3.Properties.Appearance.Options.UseFont = True
        Me.ComboBox3.Properties.Appearance.Options.UseForeColor = True
        Me.ComboBox3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox3.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat
        Me.ComboBox3.Properties.Items.AddRange(New Object() {"Qualcomm", "Mediatek", "Spreadtrum"})
        Me.ComboBox3.Properties.LookAndFeel.SkinName = "DevExpress Dark Style"
        Me.ComboBox3.Properties.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ComboBox3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox3.Size = New System.Drawing.Size(115, 20)
        Me.ComboBox3.TabIndex = 39
        '
        'panelControl7
        '
        Me.panelControl7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelControl7.Controls.Add(Me.CacheBoxAutoFormatData)
        Me.panelControl7.Controls.Add(Me.CacheBoxCreateDigest)
        Me.panelControl7.Controls.Add(Me.CekAutoRebootQc)
        Me.panelControl7.Controls.Add(Me.Btn_PatchXML)
        Me.panelControl7.Controls.Add(Me.labelControl11)
        Me.panelControl7.Controls.Add(Me.ButtonLoader)
        Me.panelControl7.Controls.Add(Me.labelControl9)
        Me.panelControl7.Controls.Add(Me.labelControl10)
        Me.panelControl7.Controls.Add(Me.TxtFlashPatchXML)
        Me.panelControl7.Controls.Add(Me.TxtFlashLoader)
        Me.panelControl7.Controls.Add(Me.Btn_RawXML)
        Me.panelControl7.Controls.Add(Me.TxtFlashRawXML)
        Me.panelControl7.Controls.Add(Me.CheckEditAutoXML)
        Me.panelControl7.Location = New System.Drawing.Point(5, 77)
        Me.panelControl7.Name = "panelControl7"
        Me.panelControl7.Size = New System.Drawing.Size(619, 103)
        Me.panelControl7.TabIndex = 27
        '
        'CacheBoxAutoFormatData
        '
        Me.CacheBoxAutoFormatData.AutoSize = True
        Me.CacheBoxAutoFormatData.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.CacheBoxAutoFormatData.CheckedColor = System.Drawing.Color.Red
        Me.CacheBoxAutoFormatData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CacheBoxAutoFormatData.ForeColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.CacheBoxAutoFormatData.Location = New System.Drawing.Point(107, 5)
        Me.CacheBoxAutoFormatData.MinimumSize = New System.Drawing.Size(0, 21)
        Me.CacheBoxAutoFormatData.Name = "CacheBoxAutoFormatData"
        Me.CacheBoxAutoFormatData.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.CacheBoxAutoFormatData.Size = New System.Drawing.Size(133, 21)
        Me.CacheBoxAutoFormatData.TabIndex = 47
        Me.CacheBoxAutoFormatData.Text = "Auto Clean Userdata"
        Me.CacheBoxAutoFormatData.UnCheckedColor = System.Drawing.Color.DarkRed
        Me.CacheBoxAutoFormatData.UseVisualStyleBackColor = False
        '
        'CacheBoxCreateDigest
        '
        Me.CacheBoxCreateDigest.AutoSize = True
        Me.CacheBoxCreateDigest.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.CacheBoxCreateDigest.CheckedColor = System.Drawing.Color.Red
        Me.CacheBoxCreateDigest.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CacheBoxCreateDigest.ForeColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.CacheBoxCreateDigest.Location = New System.Drawing.Point(367, 5)
        Me.CacheBoxCreateDigest.MinimumSize = New System.Drawing.Size(0, 21)
        Me.CacheBoxCreateDigest.Name = "CacheBoxCreateDigest"
        Me.CacheBoxCreateDigest.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.CacheBoxCreateDigest.Size = New System.Drawing.Size(126, 21)
        Me.CacheBoxCreateDigest.TabIndex = 46
        Me.CacheBoxCreateDigest.Text = "Auto Clean MiCloud"
        Me.CacheBoxCreateDigest.UnCheckedColor = System.Drawing.Color.DarkRed
        Me.CacheBoxCreateDigest.UseVisualStyleBackColor = False
        '
        'CekAutoRebootQc
        '
        Me.CekAutoRebootQc.AutoSize = True
        Me.CekAutoRebootQc.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.CekAutoRebootQc.CheckedColor = System.Drawing.Color.Red
        Me.CekAutoRebootQc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CekAutoRebootQc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.CekAutoRebootQc.Location = New System.Drawing.Point(253, 5)
        Me.CekAutoRebootQc.MinimumSize = New System.Drawing.Size(0, 21)
        Me.CekAutoRebootQc.Name = "CekAutoRebootQc"
        Me.CekAutoRebootQc.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.CekAutoRebootQc.Size = New System.Drawing.Size(108, 21)
        Me.CekAutoRebootQc.TabIndex = 45
        Me.CekAutoRebootQc.Text = "Auto Clean FRP"
        Me.CekAutoRebootQc.UnCheckedColor = System.Drawing.Color.DarkRed
        Me.CekAutoRebootQc.UseVisualStyleBackColor = False
        '
        'Btn_PatchXML
        '
        Me.Btn_PatchXML.BackColor = System.Drawing.Color.Transparent
        Me.Btn_PatchXML.FlatAppearance.BorderSize = 0
        Me.Btn_PatchXML.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_PatchXML.Image = CType(resources.GetObject("Btn_PatchXML.Image"), System.Drawing.Image)
        Me.Btn_PatchXML.Location = New System.Drawing.Point(518, 60)
        Me.Btn_PatchXML.Name = "Btn_PatchXML"
        Me.Btn_PatchXML.Size = New System.Drawing.Size(23, 13)
        Me.Btn_PatchXML.TabIndex = 41
        Me.Btn_PatchXML.UseVisualStyleBackColor = False
        '
        'labelControl11
        '
        Me.labelControl11.Location = New System.Drawing.Point(6, 60)
        Me.labelControl11.Name = "labelControl11"
        Me.labelControl11.Size = New System.Drawing.Size(54, 13)
        Me.labelControl11.TabIndex = 37
        Me.labelControl11.Text = "Scatter File"
        '
        'ButtonLoader
        '
        Me.ButtonLoader.BackColor = System.Drawing.Color.Transparent
        Me.ButtonLoader.FlatAppearance.BorderSize = 0
        Me.ButtonLoader.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonLoader.Image = CType(resources.GetObject("ButtonLoader.Image"), System.Drawing.Image)
        Me.ButtonLoader.Location = New System.Drawing.Point(518, 35)
        Me.ButtonLoader.Name = "ButtonLoader"
        Me.ButtonLoader.Size = New System.Drawing.Size(23, 13)
        Me.ButtonLoader.TabIndex = 29
        Me.ButtonLoader.UseVisualStyleBackColor = False
        '
        'labelControl9
        '
        Me.labelControl9.Location = New System.Drawing.Point(6, 36)
        Me.labelControl9.Name = "labelControl9"
        Me.labelControl9.Size = New System.Drawing.Size(51, 13)
        Me.labelControl9.TabIndex = 24
        Me.labelControl9.Text = "Raw Dump"
        '
        'labelControl10
        '
        Me.labelControl10.Location = New System.Drawing.Point(6, 84)
        Me.labelControl10.Name = "labelControl10"
        Me.labelControl10.Size = New System.Drawing.Size(43, 13)
        Me.labelControl10.TabIndex = 23
        Me.labelControl10.Text = "Raw XML"
        '
        'TxtFlashPatchXML
        '
        Me.TxtFlashPatchXML.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TxtFlashPatchXML.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.TxtFlashPatchXML.ForeColor = System.Drawing.Color.White
        Me.TxtFlashPatchXML.HintForeColor = System.Drawing.Color.Empty
        Me.TxtFlashPatchXML.HintText = ""
        Me.TxtFlashPatchXML.isPassword = False
        Me.TxtFlashPatchXML.LineFocusedColor = System.Drawing.Color.Red
        Me.TxtFlashPatchXML.LineIdleColor = System.Drawing.Color.DarkRed
        Me.TxtFlashPatchXML.LineMouseHoverColor = System.Drawing.Color.Red
        Me.TxtFlashPatchXML.LineThickness = 2
        Me.TxtFlashPatchXML.Location = New System.Drawing.Point(90, 51)
        Me.TxtFlashPatchXML.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtFlashPatchXML.Name = "TxtFlashPatchXML"
        Me.TxtFlashPatchXML.Size = New System.Drawing.Size(450, 24)
        Me.TxtFlashPatchXML.TabIndex = 42
        Me.TxtFlashPatchXML.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'TxtFlashLoader
        '
        Me.TxtFlashLoader.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TxtFlashLoader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.TxtFlashLoader.ForeColor = System.Drawing.Color.White
        Me.TxtFlashLoader.HintForeColor = System.Drawing.Color.Empty
        Me.TxtFlashLoader.HintText = ""
        Me.TxtFlashLoader.isPassword = False
        Me.TxtFlashLoader.LineFocusedColor = System.Drawing.Color.Red
        Me.TxtFlashLoader.LineIdleColor = System.Drawing.Color.DarkRed
        Me.TxtFlashLoader.LineMouseHoverColor = System.Drawing.Color.Red
        Me.TxtFlashLoader.LineThickness = 2
        Me.TxtFlashLoader.Location = New System.Drawing.Point(90, 26)
        Me.TxtFlashLoader.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtFlashLoader.Name = "TxtFlashLoader"
        Me.TxtFlashLoader.Size = New System.Drawing.Size(450, 24)
        Me.TxtFlashLoader.TabIndex = 43
        Me.TxtFlashLoader.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Btn_RawXML
        '
        Me.Btn_RawXML.BackColor = System.Drawing.Color.Transparent
        Me.Btn_RawXML.FlatAppearance.BorderSize = 0
        Me.Btn_RawXML.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Btn_RawXML.Image = CType(resources.GetObject("Btn_RawXML.Image"), System.Drawing.Image)
        Me.Btn_RawXML.Location = New System.Drawing.Point(484, 83)
        Me.Btn_RawXML.Name = "Btn_RawXML"
        Me.Btn_RawXML.Size = New System.Drawing.Size(23, 13)
        Me.Btn_RawXML.TabIndex = 42
        Me.Btn_RawXML.UseVisualStyleBackColor = False
        '
        'TxtFlashRawXML
        '
        Me.TxtFlashRawXML.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.TxtFlashRawXML.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.TxtFlashRawXML.ForeColor = System.Drawing.Color.White
        Me.TxtFlashRawXML.HintForeColor = System.Drawing.Color.Empty
        Me.TxtFlashRawXML.HintText = ""
        Me.TxtFlashRawXML.isPassword = False
        Me.TxtFlashRawXML.LineFocusedColor = System.Drawing.Color.Red
        Me.TxtFlashRawXML.LineIdleColor = System.Drawing.Color.DarkRed
        Me.TxtFlashRawXML.LineMouseHoverColor = System.Drawing.Color.Red
        Me.TxtFlashRawXML.LineThickness = 2
        Me.TxtFlashRawXML.Location = New System.Drawing.Point(90, 74)
        Me.TxtFlashRawXML.Margin = New System.Windows.Forms.Padding(4)
        Me.TxtFlashRawXML.Name = "TxtFlashRawXML"
        Me.TxtFlashRawXML.Size = New System.Drawing.Size(417, 24)
        Me.TxtFlashRawXML.TabIndex = 43
        Me.TxtFlashRawXML.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'CheckEditAutoXML
        '
        Me.CheckEditAutoXML.AutoSize = True
        Me.CheckEditAutoXML.CheckedColor = System.Drawing.Color.Red
        Me.CheckEditAutoXML.Location = New System.Drawing.Point(520, 80)
        Me.CheckEditAutoXML.MinimumSize = New System.Drawing.Size(0, 21)
        Me.CheckEditAutoXML.Name = "CheckEditAutoXML"
        Me.CheckEditAutoXML.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.CheckEditAutoXML.Size = New System.Drawing.Size(81, 21)
        Me.CheckEditAutoXML.TabIndex = 40
        Me.CheckEditAutoXML.Text = " AutoXML"
        Me.CheckEditAutoXML.UnCheckedColor = System.Drawing.Color.DarkRed
        Me.CheckEditAutoXML.UseVisualStyleBackColor = True
        '
        'ButtonFlashQc
        '
        Me.ButtonFlashQc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonFlashQc.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonFlashQc.Appearance.Options.UseBackColor = True
        Me.ButtonFlashQc.Appearance.Options.UseTextOptions = True
        Me.ButtonFlashQc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.ButtonFlashQc.AppearanceHovered.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ButtonFlashQc.AppearanceHovered.Options.UseFont = True
        Me.ButtonFlashQc.AppearanceHovered.Options.UseImage = True
        Me.ButtonFlashQc.ImageOptions.Image = CType(resources.GetObject("ButtonFlashQc.ImageOptions.Image"), System.Drawing.Image)
        Me.ButtonFlashQc.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.ButtonFlashQc.Location = New System.Drawing.Point(511, 43)
        Me.ButtonFlashQc.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ButtonFlashQc.Name = "ButtonFlashQc"
        Me.ButtonFlashQc.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.ButtonFlashQc.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.ButtonFlashQc.Size = New System.Drawing.Size(95, 28)
        Me.ButtonFlashQc.TabIndex = 28
        Me.ButtonFlashQc.Text = "WRITE DISK"
        '
        'ButtonQc_ReadP
        '
        Me.ButtonQc_ReadP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonQc_ReadP.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonQc_ReadP.Appearance.Options.UseBackColor = True
        Me.ButtonQc_ReadP.Appearance.Options.UseTextOptions = True
        Me.ButtonQc_ReadP.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.ButtonQc_ReadP.AppearanceHovered.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ButtonQc_ReadP.AppearanceHovered.Options.UseFont = True
        Me.ButtonQc_ReadP.AppearanceHovered.Options.UseImage = True
        Me.ButtonQc_ReadP.ImageOptions.Image = CType(resources.GetObject("ButtonQc_ReadP.ImageOptions.Image"), System.Drawing.Image)
        Me.ButtonQc_ReadP.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.ButtonQc_ReadP.Location = New System.Drawing.Point(23, 43)
        Me.ButtonQc_ReadP.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ButtonQc_ReadP.Name = "ButtonQc_ReadP"
        Me.ButtonQc_ReadP.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.ButtonQc_ReadP.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.ButtonQc_ReadP.Size = New System.Drawing.Size(118, 28)
        Me.ButtonQc_ReadP.TabIndex = 28
        Me.ButtonQc_ReadP.Text = "READ PARTITION"
        '
        'ButtonQc_eP
        '
        Me.ButtonQc_eP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonQc_eP.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ButtonQc_eP.Appearance.Options.UseBackColor = True
        Me.ButtonQc_eP.Appearance.Options.UseTextOptions = True
        Me.ButtonQc_eP.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.ButtonQc_eP.AppearanceHovered.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ButtonQc_eP.AppearanceHovered.Options.UseFont = True
        Me.ButtonQc_eP.AppearanceHovered.Options.UseImage = True
        Me.ButtonQc_eP.ImageOptions.Image = CType(resources.GetObject("ButtonQc_eP.ImageOptions.Image"), System.Drawing.Image)
        Me.ButtonQc_eP.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.None
        Me.ButtonQc_eP.Location = New System.Drawing.Point(284, 43)
        Me.ButtonQc_eP.LookAndFeel.UseDefaultLookAndFeel = False
        Me.ButtonQc_eP.Name = "ButtonQc_eP"
        Me.ButtonQc_eP.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light
        Me.ButtonQc_eP.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.ButtonQc_eP.Size = New System.Drawing.Size(118, 28)
        Me.ButtonQc_eP.TabIndex = 28
        Me.ButtonQc_eP.Text = "ERASE PARTITION"
        '
        'PanelDg
        '
        Me.PanelDg.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelDg.Controls.Add(Me.CkboxSelectpartitionDataView)
        Me.PanelDg.Controls.Add(Me.VScrollBarDirectISPFlashDataView)
        Me.PanelDg.Controls.Add(Me.HScrollBarDirectISPFlashDataView)
        Me.PanelDg.Controls.Add(Me.DataView)
        Me.PanelDg.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelDg.Location = New System.Drawing.Point(2, 2)
        Me.PanelDg.Name = "PanelDg"
        Me.PanelDg.Size = New System.Drawing.Size(649, 283)
        Me.PanelDg.TabIndex = 0
        '
        'CkboxSelectpartitionDataView
        '
        Me.CkboxSelectpartitionDataView.AutoSize = True
        Me.CkboxSelectpartitionDataView.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CkboxSelectpartitionDataView.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CkboxSelectpartitionDataView.Location = New System.Drawing.Point(5, 6)
        Me.CkboxSelectpartitionDataView.Name = "CkboxSelectpartitionDataView"
        Me.CkboxSelectpartitionDataView.Size = New System.Drawing.Size(12, 11)
        Me.CkboxSelectpartitionDataView.TabIndex = 40
        Me.CkboxSelectpartitionDataView.UseVisualStyleBackColor = True
        '
        'VScrollBarDirectISPFlashDataView
        '
        Me.VScrollBarDirectISPFlashDataView.Dock = System.Windows.Forms.DockStyle.Right
        Me.VScrollBarDirectISPFlashDataView.Location = New System.Drawing.Point(632, 0)
        Me.VScrollBarDirectISPFlashDataView.Name = "VScrollBarDirectISPFlashDataView"
        Me.VScrollBarDirectISPFlashDataView.Size = New System.Drawing.Size(17, 266)
        Me.VScrollBarDirectISPFlashDataView.TabIndex = 37
        '
        'HScrollBarDirectISPFlashDataView
        '
        Me.HScrollBarDirectISPFlashDataView.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.HScrollBarDirectISPFlashDataView.LargeChange = 95
        Me.HScrollBarDirectISPFlashDataView.Location = New System.Drawing.Point(0, 266)
        Me.HScrollBarDirectISPFlashDataView.Name = "HScrollBarDirectISPFlashDataView"
        Me.HScrollBarDirectISPFlashDataView.Size = New System.Drawing.Size(649, 17)
        Me.HScrollBarDirectISPFlashDataView.TabIndex = 35
        '
        'DataView
        '
        Me.DataView.AllowUserToAddRows = False
        Me.DataView.AllowUserToDeleteRows = False
        Me.DataView.AllowUserToResizeRows = False
        Me.DataView.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(44, Byte), Integer), CType(CType(44, Byte), Integer))
        Me.DataView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(70, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Column3, Me.Column5, Me.Column7, Me.Index, Me.Column6, Me.Column2, Me.Column1})
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer), CType(CType(241, Byte), Integer))
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataView.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataView.EnableHeadersVisualStyles = False
        Me.DataView.GridColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.DataView.Location = New System.Drawing.Point(0, 0)
        Me.DataView.Name = "DataView"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataView.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.DataView.RowHeadersVisible = False
        Me.DataView.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.DataView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataView.Size = New System.Drawing.Size(649, 283)
        Me.DataView.TabIndex = 39
        '
        'Column4
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.NullValue = False
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Column4.HeaderText = ""
        Me.Column4.Name = "Column4"
        Me.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column4.Width = 20
        '
        'Column3
        '
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Silver
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column3.HeaderText = "Partitions"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 80
        '
        'Column5
        '
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Silver
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column5.HeaderText = "Customs"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column5.Width = 80
        '
        'Column7
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Silver
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DarkRed
        Me.Column7.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column7.HeaderText = "Start Sectors"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 80
        '
        'Index
        '
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.GhostWhite
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White
        Me.Index.DefaultCellStyle = DataGridViewCellStyle6
        Me.Index.HeaderText = "End Sectors"
        Me.Index.Name = "Index"
        Me.Index.Width = 80
        '
        'Column6
        '
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.White
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle7
        Me.Column6.HeaderText = " Locations"
        Me.Column6.Name = "Column6"
        Me.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column6.Width = 800
        '
        'Column2
        '
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Cambria", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Silver
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle8
        Me.Column2.HeaderText = " "
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column2.Width = 5
        '
        'Column1
        '
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.DimGray
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Cambria", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Silver
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.DarkRed
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle9
        Me.Column1.HeaderText = " "
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 5
        '
        'DirectISP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.XtraFlash)
        Me.Name = "DirectISP"
        Me.Size = New System.Drawing.Size(653, 482)
        CType(Me.XtraFlash, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraFlash.ResumeLayout(False)
        CType(Me.MainTab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainTab.ResumeLayout(False)
        Me.xtraTabPage1.ResumeLayout(False)
        CType(Me.panelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelControl1.ResumeLayout(False)
        CType(Me.PanelDownload, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDownload.ResumeLayout(False)
        Me.PanelDownload.PerformLayout()
        CType(Me.ComboBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.panelControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelControl7.ResumeLayout(False)
        Me.panelControl7.PerformLayout()
        CType(Me.PanelDg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDg.ResumeLayout(False)
        Me.PanelDg.PerformLayout()
        CType(Me.DataView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents XtraFlash As PanelControl
    Private WithEvents MainTab As DevExpress.XtraTab.XtraTabControl
    Private WithEvents xtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Private WithEvents panelControl1 As PanelControl
    Private WithEvents SimpleButtonReboot As SimpleButton
    Private WithEvents PanelDownload As PanelControl
    Private WithEvents panelControl7 As PanelControl
    Private WithEvents Btn_PatchXML As Button
    Private WithEvents labelControl11 As LabelControl
    Private WithEvents ButtonLoader As Button
    Private WithEvents labelControl9 As LabelControl
    Private WithEvents labelControl10 As LabelControl
    Public WithEvents TxtFlashPatchXML As Bunifu.Framework.UI.BunifuMaterialTextbox
    Public WithEvents TxtFlashLoader As Bunifu.Framework.UI.BunifuMaterialTextbox
    Private WithEvents Btn_RawXML As Button
    Public WithEvents TxtFlashRawXML As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents CheckEditAutoXML As CacheBox
    Private WithEvents ButtonFlashQc As SimpleButton
    Private WithEvents ButtonQc_ReadP As SimpleButton
    Private WithEvents ButtonQc_eP As SimpleButton
    Private WithEvents PanelDg As PanelControl
    Private WithEvents CkboxSelectpartitionDataView As CheckBox
    Friend WithEvents VScrollBarDirectISPFlashDataView As DevExpress.XtraEditors.VScrollBar
    Private WithEvents HScrollBarDirectISPFlashDataView As DevExpress.XtraEditors.HScrollBar
    Public WithEvents DataView As DataGridView
    Private WithEvents SimpleButton3 As SimpleButton
    Friend WithEvents Column4 As DataGridViewCheckBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Index As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Private WithEvents Button1 As SimpleButton
    Private WithEvents Button6 As SimpleButton
    Private WithEvents LabelControl3 As LabelControl
    Public WithEvents ComboBox1 As ComboBoxEdit
    Private WithEvents labelControl2 As LabelControl
    Public WithEvents ComboBox3 As ComboBoxEdit
    Friend WithEvents CacheBoxAutoFormatData As CacheBox
    Friend WithEvents CacheBoxCreateDigest As CacheBox
    Friend WithEvents CekAutoRebootQc As CacheBox

End Class
